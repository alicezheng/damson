from django.conf.urls import url
from . import views
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'wifiusers', views.WifiUserViewSet)
router.register(r'hotspots', views.HotspotViewSet)

urlpatterns = [
    url(r'^login$', views.LoginView.as_view(), name='login'),
    url(r'^logout$', views.LogoutView.as_view(), name='logout'),
    url(r'^track$', views.TrackUsage.as_view(), name='track'),
]

urlpatterns += router.urls
