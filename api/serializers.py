from rest_framework import serializers
from online.models import WifiUser, Hotspot
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name']


class WifiUserSerializer(serializers.ModelSerializer):
    data_used = serializers.ReadOnlyField()
    data_remaining = serializers.ReadOnlyField()
    user = UserSerializer()

    class Meta:
        model = WifiUser
        fields = ['id', 'mac', 'data_purchased', 'data_used', 'data_remaining',
                  'user']


class HotspotSerializer(serializers.ModelSerializer):
    url = serializers.ReadOnlyField(source='get_absolute_url')
    data_used = serializers.ReadOnlyField()
    data_remaining = serializers.ReadOnlyField()

    class Meta:
        model = Hotspot
        fields = ['id', 'url', 'name', 'data_cap', 'data_used',
                  'data_remaining', 'active']
