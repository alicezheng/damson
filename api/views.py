from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route
from . import serializers
from online.models import WifiUser, Transaction, Hotspot
from traceback import print_exc
from rest_framework.authtoken.models import Token


class WifiUserViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.WifiUserSerializer
    queryset = WifiUser.objects.all()

    @list_route()
    def me(self, request):
        if self.request.user.is_anonymous():
            return Response({'error': 'You must be logged in as a WifiUser'},
                            status=400)
        serializer = self.get_serializer(self.request.user.wifiuser_set.first())
        return Response(serializer.data)

    @detail_route(methods=['post'])
    def buy_data(self, request, pk=None, *args):
        amt = request.data['amt']
        user = self.queryset.get(pk=pk)
        user.add_data(amt)
        serializer = self.get_serializer(user)
        return Response(serializer.data)

    def get_queryset(self):
        kwargs = {}

        hotspot = self.request.query_params.get('hotspot')
        if hotspot:
            kwargs['hotspotsession__hotspot'] = hotspot
            kwargs['hotspotsession__active'] = True

        return self.queryset.filter(**kwargs).distinct()


class HotspotViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.HotspotSerializer
    queryset = Hotspot.objects.all()

    def get_queryset(self):
        print self.request.data
        kwargs = {}

        serial = self.request.query_params.get('serial')
        if serial:
            kwargs['serial_code'] = serial
        if (not serial and not self.request.user.is_superuser and
            not self.request.user.is_anonymous()):
            kwargs['owner'] = self.request.user

        return self.queryset.filter(**kwargs)


class TrackUsage(APIView):
    def post(self, request, *args):
        # get rid of this wrapper when we're sure we don't need it
        try:
            return self.post_helper(request)
        except Exception as e:
            print_exc()
            return Response({'error': e.message})

    def post_helper(self, request, *args):
        data_used = int(request.data['data_used'])
        uuid = request.data['uuid']
        hotspot = request.data['hotspot']
        user = request.data['user']
        ret = Transaction.objects.track(data_used, uuid, hotspot, user)
        if ret:
            return Response('Transaction successfully logged.')
        else:
            return Response('Transaction was a duplicate and has been ignored.')


class LoginView(APIView):
    def handle_success(self, user):
        #serial = self.request.data.get('serial')
        wifiuser = user.wifiuser_set.first()
        hotspots = user.hotspot_set.all()
        tokens = Token.objects.filter(user=user)
        if not tokens.exists():
            token = Token.objects.create(user=user)
        else:
            token = tokens.first()
        ret = {'message': 'Successfully logged in.',
               'token': token.key}
        if wifiuser:
            ret['sessions'] = wifiuser.start_all_sessions()
            #session = wifiuser.start_session(serial)
            #ret['session'] = session.id
            ret['is_wifiuser'] = True
        if hotspots.exists():
            ret['is_hotspot_owner'] = True
        return ret

    def post(self, request, *args):
        username = request.data.get('username')
        if not username:
            email = request.data['email']
            u = User.objects.get(email=email)
            username = u.username
        password = request.data['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return Response(self.handle_success(user))
            else:
                return Response('This account is disabled.')
        else:
            return Response('Invalid login. Please check the username or password')


class LogoutView(APIView):
    def post(self, request, *args):
        wifiuser = request.user.wifiuser_set.first()
        if wifiuser:
            wifiuser.cancel_all_sessions()
        logout(request)
        return Response('Success.')
