from __future__ import unicode_literals
from django.db import models
from . import managers


class Hotspot(models.Model):
    owner = models.ForeignKey('auth.User', verbose_name='Owner')
    serial_code = models.CharField(max_length=32, verbose_name='Serial Code',
                                   default='', unique=True)
    name = models.CharField(max_length=30, verbose_name='Name', default='')
    data_cap = models.IntegerField(verbose_name='Data Cap (bytes)', null=True)
    active = models.BooleanField(default=True, verbose_name='Active')

    @property
    def data_used(self):
        return sum(self.hotspotsession_set.values_list('data_used', flat=True))

    @property
    def data_remaining(self):
        return (self.data_cap or 0) - self.data_used

    def __unicode__(self):
        return '%s (#%s)' % (self.name, self.pk)

    def get_absolute_url(self):
        return '/login/%s' % self.serial_code

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = "%s's Hotspot" % self.owner.username
        super(Hotspot, self).save(*args, **kwargs)


class WifiUser(models.Model):
    user = models.ForeignKey('auth.User', verbose_name='User', unique=True)
    mac = models.CharField(max_length=18, verbose_name='Mac Address')
    data_purchased = models.IntegerField(verbose_name='Data Cap (bytes)',
                                         default=0)

    def __unicode__(self):
        return '%s' % self.user.username

    def start_session(self, serial):
        hotspot = Hotspot.objects.get(serial_code=serial)
        kwargs = dict(hotspot=hotspot, user=self)
        session, created = HotspotSession.objects.get_or_create(**kwargs)
        return session

    def start_all_sessions(self):
        sessions = []
        for hotspot in Hotspot.objects.all():
            kwargs = dict(hotspot=hotspot, user=self)
            session, created = HotspotSession.objects.get_or_create(**kwargs)
            session.active = True
            session.save()
            sessions.append(session.pk)
            print session, created
        return sessions

    def cancel_all_sessions(self):
        self.hotspotsession_set.update(active=False)
        #self.hotspotsession_set.all().delete()

    def add_data(self, amt):
        self.data_purchased += amt
        self.save()

    @property
    def data_used(self):
        return sum(self.hotspotsession_set.values_list('data_used', flat=True))

    @property
    def data_remaining(self):
        return self.data_purchased - self.data_used


class HotspotSession(models.Model):
    """
    Stores data specific to a user/hotspot pair. This also represents the list
    of all hotspots that a user has authenticated with and the list of all users
    that have authenticated with a hotspot.
    TODO: rename
    """
    user = models.ForeignKey('WifiUser', verbose_name='User')
    hotspot = models.ForeignKey('Hotspot', verbose_name='Hotspot')
    data_used = models.IntegerField(verbose_name='Data Used (bytes)', default=0)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return '%s, %s' % (self.user, self.hotspot)

    def increment_data_used(self, amt):
        self.data_used += amt
        self.save()

    class Meta:
        unique_together = ('user', 'hotspot')
        index_together = ['user', 'hotspot']


class Transaction(models.Model):
    """
    This represents a single "transaction" sent from the hotspot pi to track
    amt of data used. This model ensures that the same transaction isn't sent
    twice.
    """
    data_used = models.IntegerField()
    uuid = models.CharField(max_length=16, unique=True)
    session = models.ForeignKey('HotspotSession')
    objects = managers.TransactionManager()
