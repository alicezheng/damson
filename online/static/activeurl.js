angular.module(
    'activeUrl', ['ngRoute']
).directive('activeurl', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            scope.path = function() {
                return window.location.pathname;
            };
            var exactMatch = false;
            attrs.$observe('exact', function(val) {
                if (val == '1' || val == 'true' || val == 'yes')
                    exactMatch = true;
            });
            var isActive = function() {
                var path = scope.path();
                var href = elem.attr('href') || elem.find('a').attr('href');
                if (exactMatch)
                    return path == href;
                else
                    return path.indexOf(href) == 0;
            };
            var toggleClass = function() {
                if (isActive())
                    elem.addClass('active');
                else
                    elem.removeClass('active');
            };
            toggleClass();
            scope.$on('$locationChangeSuccess', function(val) {
                if (val) {
                    toggleClass();
                }
                // collapse the menu as well
                $('#navbar').collapse('hide');
            });
        }
    }
});
