var app = angular.module('damsonApp');

/* move these to a service */
var toMb = function(num) {
    return num / 1000000;
};

var fromMb = function(num) {
   return num * 1000000;
};

var handleError = function(err) {
    console.error('failed', err);
};


/* controllers */

app.controller('OwnerLoginCtrl', function OwnerLoginCtrl($scope, LoginService) {
    $scope.submit = function() {
        LoginService.login($scope.email, $scope.password, $scope.remember)
            .then(function(data) {
                LoginService.redirectToHome();
            });
    };
});

app.controller('UserLoginCtrl', function UserLoginCtrl($scope, LoginService, $routeParams, $http) {
    var redirect = LoginService.redirectToHome;
    $scope.submit = function() {
        LoginService.login($scope.email, $scope.password, $scope.remember, $routeParams.serial)
            .then(function(data) {
                redirect();
            });
    };
});

app.controller('LogoutCtrl', function LogoutCtrl($scope, $timeout, LoginService) {
    console.log('logout');
    var redirect = function() {
        window.location = '/';
    };
    LoginService.logout().then(function(res) {
        $scope.success = true;
        $timeout(redirect, 1000);
    });
});

app.controller('HomeCtrl', function HomeCtrl($scope, $http, LoginService) {
    var host = window.location.hostname;
    if (!host.includes('damson')&&!host.includes('localhost'))
	window.location = 'http://damson.online/';
    LoginService.redirectToHome();
});


app.controller('UserAccountCtrl', function UserAccountCtrl($scope, LoginService, ModalService, $http, $interval, DamsonData) {
    var getMax = function() {
        return Math.max($scope.user.data_purchased_mb,
                        $scope.user.data_used_mb);
    };
    var getRemaining = function() {
        return $scope.user.data_purchased_mb - $scope.user.data_used_mb;
    };
    var replaceUser = function(res) {
        $scope.user = res.data;
        updateUser(res);
    };

    var updateUser = function(res) {
        $scope.showSuccess = false; // hax
        $scope.user.data_purchased = res.data.data_purchased;
        $scope.user.data_used = res.data.data_used;
        $scope.user.data_remaining = res.data.data_remaining;
        $scope.user.data_purchased_mb = toMb($scope.user.data_purchased);
        $scope.user.data_used_mb = toMb($scope.user.data_used)
        $scope.user.getMax = getMax;
        $scope.user.getRemaining = getRemaining;
    };
    var getData = function(initial) {
        $http.get('/api/wifiusers/me').then(function(res) {
            if (initial===true)
                return replaceUser(res);
            else
                return updateUser(res)
        });
    };
    if (LoginService.isLoggedIn)
        getData(true);
    else
        window.location = '/login/' + $localStorage.serial;

    // live update on data
    $interval(getData,5000);

    $scope.updateMac = function() {
        $http.patch('/api/wifiusers/' + $scope.user.id + '/', {mac: $scope.user.mac})
            .then(function(res) {
                console.log('updated mac', res);
                $scope.showSuccess = true;
            }, function(err) {
                console.error('failed to update mac', err);
            });
    };

    $scope.buyData = function() {
        ModalService.showModal({
            templateUrl: "partials/buydata/",
            controller: "BuyDataController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                console.log('amt', result);
                $http.post('/api/wifiusers/' + $scope.user.id + '/buy_data/',
                           {amt: result})
                    .then(updateUser);
            });
        });
    };


    // Incorporating credits
    $scope.totalDataMax = function() {
        if ($scope.user) {
            if ($scope.totalData.used)
                return Math.max(
                    $scope.user_data_purchased_mb + $scope.totalData.used(),
                    $scope.user.data_used_mb);
            else return $scope.user.getMax();
        }
    };
    $scope.totalDataRemaining = function() {
        if ($scope.user) {
            var ret = $scope.user.getRemaining();
            if ($scope.totalData.used)
                ret += $scope.totalData.used();
            return ret
        }
    };
    $scope.isHotspotOwner = LoginService.isHotspotOwner();
    if ($scope.isHotspotOwner) {
        DamsonData.init();
        $scope.ownerData = DamsonData;
        $scope.totalData = DamsonData.totalData;
    }
    else $scope.totalData = {};
});


app.controller('BuyDataController', ['$scope', 'close', function($scope, close) {
    $scope.amt = 10;

    $scope.cancel = function() {
        close(0, 500); // close, but give 500ms for bootstrap to animate
    };

    $scope.submit = function() {
        close($scope.amt * 1000000, 500);
    };

}]);


app.controller('ConfigCtrl', function($scope, LoginService, DamsonData) {

    console.log('hotspot config');
    $scope.data = DamsonData;
    $scope.$watch('data.hotspots', function(val) {
        $scope.hotspots = val;
    });
    $scope.totalData = $scope.data.totalData;

    if (LoginService.isLoggedIn)
        $scope.data.init();
    else
        window.location = '/login/';

    $scope.update = function(hotspot) {
        var data = {
            name: hotspot.name,
            data_cap: fromMb(hotspot.data_cap_mb)
        };
        hotspot.patch(data)
            .then(hotspot.handleSuccess, handleError);
    };

    $scope.disableAll = function() {
        _.each($scope.hotspots, function(hotspot) {
            hotspot.disable();
        });
    };

    $scope.addNew = function() {
        alert('Not yet implemented');
    };

});


app.controller('AccountCtrl', function($scope) {
    console.log('toggle between hotspot config and wifiuser');
    $scope.selected = 'wifiuser';
    $scope.select = function(type) {
        $scope.selected = type;
    };
});
