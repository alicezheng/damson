angular.module('damsonApp')
    .factory('DamsonData', function($http, $interval, $timeout) {
        var service = {};

        /* hotspot methods */
        var disable = function() {
            var hotspot = this;
            this.patch({active: false})
                .then(hotspot.handleSuccess, handleError);
        };
        var enable = function() {
            var hotspot = this;
            this.patch({active: true})
                .then(hotspot.handleSuccess, handleError);
        };
        var getRemaining = function() {
            return this.data_cap - this.data_used;
        };
        var getMax = function() {
            return Math.max(this.data_cap_mb,
                            this.data_used_mb);
        };
        var patch = function(data) {
            console.log('patch', data);
            return $http.patch('/api/hotspots/' + this.id + '/', data);
        };

        var getSuccessHandler = function(hotspot) {
            return function(res) {
                console.log('successful update', res, hotspot);
                hotspot.success = true;
                $timeout(function() { hotspot.success = false; }, 5000);
                hotspot.data_used = res.data.data_used;
                hotspot.data_cap = res.data.data_cap;
                hotspot.data_used_mb = toMb(res.data.data_used);
                hotspot.data_cap_mb = toMb(res.data.data_cap);
                hotspot.name = res.data.name;
                hotspot.active = res.data.active;
                console.log('current state', hotspot);
            }
        };

        /* other stuff */
        var handleError = function(err) {
            console.error('failed', err);
        };
        var getData = function(initial) {
            var callback = function(initial) {
                if (initial === true)
                    return function(res) {
                        console.log('hotspots', res, initial === true);
                        service.hotspots = _.map(res.data, function(datum) {
                            datum.getRemaining = getRemaining;
                            datum.getMax = getMax;
                            datum.data_used_mb = toMb(datum.data_used);
                            datum.data_cap_mb = toMb(datum.data_cap);
                            datum.patch = patch;
                            datum.disable = disable;
                            datum.enable = enable;
                            datum.handleSuccess = getSuccessHandler(datum);
                            return datum;
                        });
                    };
                else return function(res) {
                    // assume same # of hotspots for now, only update data_used
                    _.each(service.hotspots, function(hotspot, i) {
                        var datum = res.data[i];
                        hotspot.data_used = datum.data_used;
                        hotspot.data_used_mb = toMb(datum.data_used);
                    });
                    console.log('updated total used is now', service.totalData.used());

                };
            }(initial);
            $http.get('/api/hotspots/')
                .then(callback, handleError);
        };

        service.getData = getData;
        service.totalData = {
            used: function() {
                var sum = _.reduce(service.hotspots, function(memo, hotspot) {
                    return memo + hotspot.data_used;
                }, 0);
                return toMb(sum);
            },
            remaining: function() {
                var sum = _.reduce(service.hotspots, function(memo, hotspot) {
                    return memo + hotspot.getRemaining();
                }, 0);
                return toMb(sum);
            },
            cap: function() {
                var sum = _.reduce(service.hotspots, function(memo, hotspot) {
                    return memo + hotspot.data_cap;
                }, 0);
                return toMb(sum);
            },
            max: function() {
                return Math.max(this.cap(), this.used());
            }
        };
        service.init = function() {
            service.getData(true);
            service.interval = $interval(getData, 5000);
        };

        return service;
    })
    .factory('LoginService', function($http, $localStorage) {
        var service = {};
        service.login = function(email, password, remember, serial) {
            var data = {email: email,
                        password: password,
                        remember: remember};
            if (serial) {
                data.serial = serial;
                $localStorage.serial = serial;
            }
            return $http.post('/api/login', data)
                .then(function(res) {
                    console.log('logged in', res);
                    $localStorage.authtoken = res.data.token;
                    //$localStorage.session = res.data.session;
                    $localStorage.sessions = res.data.sessions;
                    $localStorage.isWifiUser = res.data.is_wifiuser;
                    $localStorage.isHotspotOwner = res.data.is_hotspot_owner;
                    return data;
                }, function(error) {
                    console.error('failed to login', error);
                });

        };
        service.redirectToHome = function() {
            if (this.isLoggedIn()) {
                console.log('redirect based on type of user');
                if (this.isWifiUser() && this.isHotspotOwner())
                    window.location = '/home';
                else if (this.isWifiUser())
                    window.location = '/account';
                else if (this.isHotspotOwner())
                    window.location = '/config';
                else
                    console.log('not a wifi user or hotspot owner');
            }
        };
        service.isLoggedIn = function() {
            console.log('authtoken', $localStorage.authtoken);
            return $localStorage.authtoken !== null;
        };
        service.isWifiUser = function() {
            return $localStorage.isWifiUser === true;
        };
        service.isHotspotOwner = function() {
            return $localStorage.isHotspotOwner === true;
        };
        service.logout = function() {
            return $http.post('/api/logout')
                .then(function(data) {
                    console.log('logged out', data);
                    $localStorage.$reset();
                }, function(error) {
                    console.error('failed to logout', error);
                });
        };

        return service;
    });
