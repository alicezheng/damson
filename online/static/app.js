var app = angular.module('damsonApp', ['ngRoute', 'activeUrl', 'ngStorage', 'angular-svg-round-progressbar', 'angularModalService']);

app.config(['$locationProvider', '$routeProvider', '$httpProvider',
    function config($locationProvider, $routeProvider, $httpProvider) {
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.interceptors.push(function($q, $localStorage) {
            return {
                'request': function(config) {
                    if ($localStorage.authtoken)
                        config.headers['Authorization'] = 'Token ' + $localStorage.authtoken;
                    return config;
                },
            };
        });
        $locationProvider.html5Mode(true);
        $routeProvider.
            when('/', {
                // redirect to appropriate homepage
                controller: 'HomeCtrl',
                templateUrl: 'partials/home/'
            }).
            when('/home', {
                // combined WifiUser & Hotspot config
                controller: 'AccountCtrl',
                templateUrl: 'partials/account/'
            }).
            when('/account', {
                // WifiUser account
                controller: 'UserAccountCtrl',
                templateUrl: 'partials/useraccount/'
            }).
            when('/config', {
                // Hotspot config page
                controller: 'ConfigCtrl',
                templateUrl: 'partials/hotspot/'
            }).
            when('/login/:serial', {
                // WifiUser facing
                controller: 'UserLoginCtrl',
                templateUrl: 'partials/login/'
            }).
            when('/login', {
                // Hotspot owner facing
                controller: 'OwnerLoginCtrl',
                templateUrl: 'partials/login/'
            }).
            when('/logout', {
                controller: 'LogoutCtrl',
                templateUrl: 'partials/logout/'
            }).
            otherwise('/');
    }]);
