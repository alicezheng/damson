from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, DetailView
from .models import Hotspot


class PartialView(TemplateView):
    app_name = None
    partial_dir = 'partials'

    def get_template_names(self):
        from django.core.urlresolvers import resolve
        page_id = self.kwargs.get('page_id')
        match = resolve(self.request.path)
        app_name = self.app_name or match.app_name or match.namespace
        if page_id:
            if app_name:
                template = '%s/%s/%s.html' % (app_name, self.partial_dir,
                                              page_id)
            else:
                template = '%s/%s.html' % (self.partial_dir, page_id)
            return template



class HomeView(TemplateView):
    """
    Main view -- all client-side routing considerations will be handled by angular.
    """
    template_name = 'online/home.html'
