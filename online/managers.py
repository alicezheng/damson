from django.db import models


class TransactionManager(models.Manager):
    def track(self, data_used, uuid, hotspot_id, wifiuser_id):
        from .models import HotspotSession
        session = HotspotSession.objects.get(user_id=wifiuser_id,
                                             hotspot_id=hotspot_id)
        if self.filter(uuid=uuid).exists():
            # this is a duplicate, do nothing
            return
        else:
            session.increment_data_used(data_used)
            return self.create(data_used=data_used, uuid=uuid, session=session)
