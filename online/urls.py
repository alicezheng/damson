from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='index'),
    url(r'^login/(?P<serial>\w+)/$', views.HomeView.as_view(), name='portal'),
    url(r'^(?P<page_id>\w+)/$', views.HomeView.as_view(), name='page'),
    url(r'^partials/(?P<page_id>\w+)/$', views.PartialView.as_view(), name='partial'),
]
