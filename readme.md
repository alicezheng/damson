# Getting Started

1.  ```mkdir damson_root```
2. ```virtualenv damson_root```
3. ```cd damson_root```
4. ```
source bin/activate
```
5. ```git clone git@bitbucket.org:alicezheng/damson.git```
6. ```cd damson```
7. ```pip install -r requirements.txt```