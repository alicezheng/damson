import time
import requests
import uuid
import threading
import subprocess
#from fysom import Fysom

#global variables
hotspot = 1
current = {}
new = {}
payload = {'hotspot':'1'}
serial = 'abcd'


def update_rules(new, current):
    add_rule_user = {}
    remove_rule_user = {}

    #compared dicts and update the remove or add user rule dict. 
    for key in current:
        if key not in new:
            remove_rule_user[key] = current[key]

    for key in new:
        if key not in current:
            add_rule_user[key] = new[key]

    #set current to the new dict 
    current = new

    #Code to check the balance, if 0 add them to remove rule. 
    for key in current:
        if key['data_remaining'] == 0:
            remove_rule_user[key] = current[key]

    return (remove_rule_user, add_rule_user)


def add_rule(rule):
    #add rule
    print "added"


def remove_rule(rule):
    #remove rule
    print "removed"


def update_bandwith(current):

    #wait for ok, reset the data. 
    for user in current:
        up_payload = {  'uuid':uuid.uuid1().bytes, 
                        'data_used':'3', 
                        'hotspot': hotspot, 
                        'user': user
                    }
        r = requests.post('http://damson.online/api/track', data=up_payload)
        if r.json() == 'Transaction successfully logged.'
            print "logged"
            #RESET the data on firewall


class BashIP(threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name

    def run(self):
        print "up"
        for i in range(10,16):
            ip = "192.168.42.%d" %(i)
            subprocess.Popen('./rmtrack.sh %s' % (ip), shell=True)
            #Call bash script



if __name__ == "__main__":

    #get login url for the firewall
    r = requests.get('http://damson.online/api/hotspots/', params = {'serial':serial})
    if r.json():
        login_url = 'http://damson.online/api/hotspots' + r.json()[0]['url']
    else:
        #TODO exit with an error that hotspot isn't registered
        print "wrong serial"

    bash_ip = BashIP(1, "bash")
    bash_ip.start()

    #get initial list and add
    add_rule_user = {}

    r = requests.get('http://damson.online/api/wifiusers', params = payload)
    user_list = r.json()
    for user in user_list:
        current[user['id']] = {'mac':user['mac'], 'balance':user['data_remaining']}
    for key in current:
        add_rule_user[key] = current[key]


    #function to add. 
    print "first user added", add_rule_user

    #loop
    while(1):
        
        r = requests.get('http://damson.online/api/wifiusers', params = payload)
        user_list = r.json()
        #GET request for a list of users
        for user in user_list:
            new[user['id']] = {'mac':user['mac'], 'balance':user['data_remaining']}
        
        remove_rule_user, add_rule_user = update_rules(new, current)
        
        #update the firewall
        
        print remove_rule_user, add_rule_user
        #track the current array usage, and transmit to the server. 


        time.sleep(1)








